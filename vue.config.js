module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],

  publicPath: process.env.NODE_ENV === 'production' ? '/admin-bluevet/' : '/',

  pluginOptions: {
    cordovaPath: 'src-cordova'
  }
}



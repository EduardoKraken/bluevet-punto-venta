import Vue from 'vue'
import { mapGetters,mapActions } from 'vuex';

import XLSX from 'xlsx'

export default {
	methods: {
		/* EXPORTAR TODOS LOS CONTACTOS */
    exportarContactos () {
      const dataExport = []
      for(const i in this.contactExport){
        dataExport.push({
          nombre             :  this.contactExport[i].folio,
          'Given Name'       : this.contactExport[i].folio,
          'Family Name'      : this.contactExport[i].nombre_completo,
          'Group Membership' : '* myContacts',
          'Phone 1 - Type'   : 'Mobile',
          'Phone 1 - Value'  : this.contactExport[i].telefono,
        })
      }

      this.exportExcel(dataExport, 'contacts_google')
    },

    exportExcel(dataExport, name){
      let data = XLSX.utils.json_to_sheet(dataExport)
      const workbook = XLSX.utils.book_new()
      const filename = name
      XLSX.utils.book_append_sheet(workbook, data, filename)

      var wbout = XLSX.write(workbook, {
        bookType: 'csv',
        bookSST: false,
        type: 'binary'
      });

      const file = new File([new Blob([this.s2ab(wbout)])], filename + '.csv')
      
      let formData = new FormData();
      // //se crea el objeto y se le agrega como un apendice el archivo 
      formData.append('file',file);
      // /*getDatosRiesgo the form data*/

      this.$http.post('pdfs',formData).then(response=> {
        window.location = this.$http.options.root + 'pdfs/' + filename + '.csv'
      }).catch(error=> {
        console.log(error);
      });
    },

    s2ab(s) {
      var buf = new ArrayBuffer(s.length);
      var view = new Uint8Array(buf);
      for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    },

  }
}
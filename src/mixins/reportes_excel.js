import Vue from 'vue'
import XLSX from 'xlsx'

export default {
	methods: {
		s2ab(s) {
        var buf = new ArrayBuffer(s.length);
        var view = new Uint8Array(buf);
        for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
        return buf;
      },

      exportarExcel(nombreArchivo, datos){
        let data = XLSX.utils.json_to_sheet(datos)
        const workbook = XLSX.utils.book_new()
        const filename = nombreArchivo
        XLSX.utils.book_append_sheet(workbook, data, filename)

        var wbout = XLSX.write(workbook, {
          bookType: 'xls',
          bookSST: false,
          type: 'binary'
        });

        const file = new File([new Blob([this.s2ab(wbout)])], filename + '.xls')
        
        let formData = new FormData();
        // //se crea el objeto y se le agrega como un apendice el archivo 
        formData.append('file',file);
        // /*getDatosRiesgo the form data*/

        this.$http.post('reportes_excel',formData).then(response=> {
          window.location = this.$http.options.root + 'reportes/' + filename + '.xls'
        }).catch(error=> {
          console.log(error);
        });
      },

  }
}